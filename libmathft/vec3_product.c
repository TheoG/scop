/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3_product.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 13:38:13 by tgros             #+#    #+#             */
/*   Updated: 2017/03/15 15:46:25 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

/*
** Scales a vector by i.
*/

t_vec3	v3_prod(t_vec3 vec1, double i)
{
	t_vec3	vec;

	vec.x = vec1.x * i;
	vec.y = vec1.y * i;
	vec.z = vec1.z * i;
	return (vec);
}
