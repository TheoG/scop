/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate_matrix4.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 10:09:16 by tgros             #+#    #+#             */
/*   Updated: 2018/04/18 13:59:17 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"
#include <stdio.h>

/*
** Creates a new rotation matrix of angle 'angle' around axis 'axis'
*/

void				init_angle(t_vec3 *cos_a, t_vec3 *sin_a, t_vec3 *angle)
{
	cos_a->x = cos(to_radian(angle->x));
	sin_a->x = sin(to_radian(angle->x));
	cos_a->y = cos(to_radian(angle->y));
	sin_a->y = sin(to_radian(angle->y));
	cos_a->z = cos(to_radian(angle->z));
	sin_a->z = sin(to_radian(angle->z));
}

t_matrix4			rotate_matrix4_v3(t_matrix4 m, t_vec3 angle)
{
	t_vec3	cos;
	t_vec3	sin;

	init_angle(&cos, &sin, &angle);
	m[0] = cos.y * cos.z;
	m[1] = (cos.z * sin.x * sin.y) - (cos.x * sin.z);
	m[2] = (cos.x * cos.z * sin.y) + (sin.x * sin.z);
	m[4] = cos.y * sin.z;
	m[5] = (cos.x * cos.z) + (sin.x * sin.z * sin.y);
	m[6] = (cos.x * sin.y * sin.z) - (cos.z * sin.x);
	m[8] = -sin.y;
	m[9] = cos.y * sin.x;
	m[10] = cos.x * cos.y;
	return (m);
}

t_matrix4			rotate_matrix4_av3(t_matrix4 m, float angle, t_vec3 u)
{
	angle = to_radian(angle);
	m[0] = cos(angle) + (u.x * u.x) * (1 - cos(angle));
	m[1] = u.x * u.y * (1 - cos(angle)) - u.z * sin(angle);
	m[2] = u.x * u.z * (1 - cos(angle)) + u.y * sin(angle);
	m[4] = u.y * u.x * (1 - cos(angle)) + u.z * sin(angle);
	m[5] = cos(angle) + u.y * u.y * (1 - cos(angle));
	m[6] = u.y * u.z * (1 - cos(angle)) - u.x * sin(angle);
	m[8] = u.z * u.x * (1 - cos(angle)) - u.y * sin(angle);
	m[9] = u.z * u.y * (1 - cos(angle)) + u.x * sin(angle);
	m[10] = cos(angle) + u.z * u.z * (1 - cos(angle));
	return (m);
}
