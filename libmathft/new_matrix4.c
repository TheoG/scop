/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_matrix4.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 10:55:26 by tgros             #+#    #+#             */
/*   Updated: 2018/04/09 15:02:36 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

/*
** Creates a new 4 by 4 matrix and sets its values to 0;
*/

t_matrix4	new_matrix4(void)
{
	int			y;
	t_matrix4	m;

	if (!(m = (t_matrix4)malloc(sizeof(double *) * 16)))
		return (NULL);
	y = -1;
	while (++y < 16)
	{
		m[y] = 0;
	}
	return (m);
}
