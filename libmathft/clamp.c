/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clamp.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 13:43:35 by tgros             #+#    #+#             */
/*   Updated: 2017/03/15 15:45:00 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

double		f_clamp(double nb, double min, double max)
{
	nb < min ? nb = min : 0;
	nb > max ? nb = max : 0;
	return (nb);
}

int			i_clamp(int *nb, int min, int max)
{
	*nb < min ? *nb = min : 0;
	*nb > max ? *nb = max : 0;
	return (*nb);
}

t_color		c_clamp(t_color *nb, int min, int max)
{
	(*nb).r < min ? (*nb).r = min : 0;
	(*nb).r > max ? (*nb).r = max : 0;
	(*nb).g < min ? (*nb).g = min : 0;
	(*nb).g > max ? (*nb).g = max : 0;
	(*nb).b < min ? (*nb).b = min : 0;
	(*nb).b > max ? (*nb).b = max : 0;
	return (*nb);
}
