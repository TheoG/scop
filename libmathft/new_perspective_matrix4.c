/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_perspective_matrix4.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 16:40:36 by tgros             #+#    #+#             */
/*   Updated: 2018/04/18 14:35:44 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

/*
** Creates a new 4 by 4 projection matrix.
*/

t_matrix4	new_perspective_matrix4(float fov, float aspect_ratio,
														float near, float far)
{
	t_matrix4	m;

	m = new_matrix4();
	fov = (fov / 180) * M_PI;
	m[0] = 1.0 / (aspect_ratio * tan(fov / 2));
	m[5] = 1.0 / (tan(fov / 2));
	m[10] = -((far - near) / (far - near));
	m[11] = -1;
	m[14] = -(2 * far * near) / (far - near);
	return (m);
}
