/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate_matrix4.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 10:09:16 by tgros             #+#    #+#             */
/*   Updated: 2018/04/18 14:00:14 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

static	void		m_rot_x(t_matrix4 m, double angle)
{
	m[0] = 1;
	m[5] = cos(to_radian(angle));
	m[6] = sin(to_radian(angle));
	m[9] = -sin(to_radian(angle));
	m[10] = cos(to_radian(angle));
	m[15] = 1;
}

static	void		m_rot_y(t_matrix4 m, double angle)
{
	m[0] = cos(to_radian(angle));
	m[2] = -sin(to_radian(angle));
	m[5] = 1;
	m[8] = sin(to_radian(angle));
	m[10] = cos(to_radian(angle));
	m[15] = 1;
}

static	void		m_rot_z(t_matrix4 m, double angle)
{
	m[0] = cos(to_radian(angle));
	m[1] = sin(to_radian(angle));
	m[4] = -sin(to_radian(angle));
	m[5] = cos(to_radian(angle));
	m[10] = 1;
	m[15] = 1;
}

static	void		reset_m(t_matrix4 m)
{
	int	i;

	i = 0;
	while (i < 16)
	{
		if (i == 0 || i == 5 || i == 10 || i == 15)
			m[i] = 1;
		else
			m[i] = 0;
		i++;
	}
}

/*
** Creates a new rotation matrix of angle 'angle' around axis 'axis'
*/

t_matrix4			rotate_matrix4(t_matrix4 m, double angle, char axis)
{
	reset_m(m);
	if (axis == 'x' || axis == 'X')
		m_rot_x(m, angle);
	else if (axis == 'y' || axis == 'Y')
		m_rot_y(m, angle);
	else if (axis == 'z' || axis == 'Z')
		m_rot_z(m, angle);
	return (m);
}
