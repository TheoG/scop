/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_rotation_matrix4.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 10:55:34 by tgros             #+#    #+#             */
/*   Updated: 2018/04/09 15:50:41 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

static t_matrix4	m_rot_x(double angle)
{
	t_matrix4	m;

	m = new_matrix4();
	m[0] = 1;
	m[5] = cos(to_radian(angle));
	m[6] = sin(to_radian(angle));
	m[9] = -sin(to_radian(angle));
	m[10] = cos(to_radian(angle));
	m[15] = 1;
	return (m);
}

static t_matrix4	m_rot_y(double angle)
{
	t_matrix4	m;

	m = new_matrix4();
	m[0] = cos(to_radian(angle));
	m[2] = -sin(to_radian(angle));
	m[5] = 1;
	m[8] = sin(to_radian(angle));
	m[10] = cos(to_radian(angle));
	m[15] = 1;
	return (m);
}

static t_matrix4	m_rot_z(double angle)
{
	t_matrix4	m;

	m = new_matrix4();
	m[0] = cos(to_radian(angle));
	m[1] = sin(to_radian(angle));
	m[4] = -sin(to_radian(angle));
	m[5] = cos(to_radian(angle));
	m[10] = 1;
	m[15] = 1;
	return (m);
}

/*
** Creates a new rotation matrix of angle 'angle' around axis 'axis'
*/

t_matrix4			new_rotation_matrix4(double angle, char axis)
{
	if (axis == 'x' || axis == 'X')
		return (m_rot_x(angle));
	else if (axis == 'y' || axis == 'Y')
		return (m_rot_y(angle));
	else if (axis == 'z' || axis == 'Z')
		return (m_rot_z(angle));
	return (0);
}
