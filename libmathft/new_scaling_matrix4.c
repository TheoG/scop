/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_scaling_matrix4.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 10:56:20 by tgros             #+#    #+#             */
/*   Updated: 2018/04/09 15:04:48 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

/*
** Creates a new scaling matrix which will scale a matrix or vector of all
** coordinates by i.
*/

t_matrix4	new_scaling_matrix4(double i)
{
	t_matrix4	m;

	m = new_matrix4();
	m[0] = i;
	m[4] = i;
	m[8] = i;
	m[15] = i;
	return (m);
}
