/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3_divide.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 13:37:42 by tgros             #+#    #+#             */
/*   Updated: 2017/03/15 15:46:07 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

/*
** Divide a vector by i.
*/

t_vec3	v3_div(t_vec3 vec1, double i)
{
	t_vec3	vec;

	vec.x = vec1.x / i;
	vec.y = vec1.y / i;
	vec.z = vec1.z / i;
	return (vec);
}
