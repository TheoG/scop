/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/08 13:39:57 by tgros             #+#    #+#             */
/*   Updated: 2017/03/15 15:45:06 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

t_color		col_add(t_color *c1, t_color *c2)
{
	c1->r += c2->r;
	c1->g += c2->g;
	c1->b += c2->b;
	return (*c1);
}

t_color		col_addf(t_color *c1, double r, double g, double b)
{
	c1->r += r;
	c1->g += g;
	c1->b += b;
	return (*c1);
}

t_color		col_sub(t_color *c1, t_color *c2)
{
	c1->r -= c2->r;
	c1->g -= c2->g;
	c1->b -= c2->b;
	return (*c1);
}
