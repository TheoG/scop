/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libmathft.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 10:54:30 by tgros             #+#    #+#             */
/*   Updated: 2018/04/20 11:06:32 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBMATHFT_H
# define LIBMATHFT_H

# include <math.h>
# include <stdlib.h>

typedef struct	s_pt2
{
	int			x;
	int			y;
}				t_pt2;

typedef struct	s_pt3
{
	int			x;
	int			y;
	int			z;
}				t_pt3;

typedef struct	s_pt4
{
	int			x;
	int			y;
	int			z;
	int			w;
}				t_pt4;

typedef struct	s_vec2
{
	float		x;
	float		y;
}				t_vec2;

typedef	struct	s_dpt2
{
	double		x;
	double		y;
}				t_dpt2;

typedef struct	s_vec3
{
	float		x;
	float		y;
	float		z;
}				t_vec3;

typedef struct	s_complex
{
	double		r;
	double		i;
}				t_complex;

typedef struct	s_color
{
	int			r;
	int			g;
	int			b;
	int			a;
}				t_color;

typedef float	*t_matrix4;

double			to_radian(double a);
int				ft_round(double i);
int				ft_rgb_mix(int *colors, int n);
int				quadratic(double a, double b, double c, t_vec2 *res);
void			double_swap(double *f1, double *f2);
int				int_max(int a, int b);
double			double_max(double a, double b);
int				int_min(int a, int b);
double			double_min(double a, double b);
double			f_clamp(double nb, double min, double max);
int				i_clamp(int *nb, int min, int max);
t_color			c_clamp(t_color *nb, int min, int max);

/*
** 3 x 3 Vector functions
*/

double			v3_dot(t_vec3 vec1, t_vec3 vec2);
double			v3_len(t_vec3 vec);
double			v3_dist(t_vec3 v1, t_vec3 v2);
t_vec3			new_v3(float x, float y, float z);
t_vec2			new_v2(float x, float y);
t_vec3			v3_nrm(t_vec3 vec);
t_vec3			v3_cross(t_vec3 vec1, t_vec3 vec2);
t_vec3			v3_add(t_vec3 vec1, t_vec3 vec2);
t_vec3			v3_sub(t_vec3 vec1, t_vec3 vec2);
t_vec3			v3_prod(t_vec3 vec1, double i);
t_vec3			v3_div(t_vec3 vec1, double i);
t_vec3			vec3_translate(t_vec3 vec, t_vec3 i);
t_vec3			vec3_scale(t_vec3 vec, t_vec3 i);
t_vec3			v3_m4_prod(t_vec3 p, t_matrix4 m);

t_pt3			new_pt3(int x, int y, int z);

/*
** 4 x 4 Matrix functions
*/

t_matrix4		new_matrix4(void);
t_matrix4		new_identity_matrix4(void);
t_matrix4		new_perspective_matrix4(float fov, float ar, float n, float f);
t_matrix4		new_scaling_matrix4(double i);
t_matrix4		m4_prod(t_matrix4 m1, t_matrix4 m2);
t_matrix4		m4_prod_r(t_matrix4 m1, t_matrix4 m2, t_matrix4 m);
t_matrix4		new_rotation_matrix4(double angle, char axis);
t_matrix4		m4_trans(t_matrix4 m, t_vec3 v);
t_matrix4		rotate_matrix4(t_matrix4 m, double angle, char axis);
t_matrix4		rotate_matrix4_v3(t_matrix4 m, t_vec3 angle);
t_matrix4		rotate_matrix4_av3(t_matrix4 m, float angle, t_vec3 v);
void			matrix4_cpy(t_matrix4 dst, t_matrix4 src);
void			matrix4_free(t_matrix4 m);

/*
** Complex numbers functions
*/

t_complex		c_add(t_complex x, t_complex y);
t_complex		c_minus(t_complex x, t_complex y);
t_complex		c_minus_double(t_complex x, double y);
t_complex		c_add_float(t_complex x, float y, float z);
t_complex		c_product(t_complex x, t_complex y);
t_complex		c_product_double(t_complex c, double y);
t_complex		c_divide(t_complex x, t_complex y);
double			c_modulus(t_complex x);
t_complex		c_abs(t_complex x);

/*
** Colors functions
*/

t_color			col_add(t_color *c1, t_color *c2);
t_color			col_addf(t_color *c1, double r, double g, double b);
t_color			col_sub(t_color *c1, t_color *c2);

#endif
