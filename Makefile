# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tgros <tgros@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/28 14:36:13 by tgros             #+#    #+#              #
#    Updated: 2018/04/20 10:48:42 by tgros            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = scop
SRC_PATH = src
INC_PATH = inc/
OBJ_PATH = obj
LIBFT_PATH = libft/
LIBMATHFT_PATH = libmathft/

GLFW_PATH = glfw/
GLFW_DEPS = $(GLFW_PATH)deps/
GLFW_INCLUDE = $(GLFW_PATH)include/
GLAD_FILE = glad.c
GLAD_OBJ = $(addprefix $(OBJ_PATH)/, $(GLAD_FILE:.c=.o))

GLFW_STUFF = -I glfw/include glfw/lib/libglfw3.a -framework OpenGL -framework Cocoa -framework IOKit -framework CoreVideo

LIBFT_NAME = libft.a
LIBMATHFT_NAME = libmathft.a

HEADER_FILE = scop.h

SRC_NAME = 	main \
            read_obj \
            shader \
			read_bmp \
			read_face_1 \
			read_face_2 \
			texture \
			input \
			util \
			window \
			matrices \
			vertices \
			triangles \
			texture_vertices \
			center_obj \
			loop \
			scop

SRCNAME_PATH = $(addprefix $(OBJ_PATH), $(SRC_NAME))
SRC = $(addsuffix $(EXT), $(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH)/, $(SRC:.c=.o))

EXT = .c
CC	= clang
ECHO = echo
FLG = -Werror -Wextra -Wall

all: $(NAME)

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@/bin/mkdir -p $(OBJ_PATH)
	@$(CC) $(FLG) -I$(INC_PATH) -I$(GLFW_INCLUDE) -I$(GLFW_DEPS)/ -I$(LIBFT_PATH) -I$(LIBMATHFT_PATH) -c -o $@ $^

$(NAME): $(OBJ) $(INC_PATH)$(HEADER_FILE)
	@make -C $(LIBFT_PATH)
	@make -C $(LIBMATHFT_PATH)
	@$(CC) $(FLG) glfw/deps/glad.c -I$(GLFW_INCLUDE) -I$(GLFW_DEPS) -c -o $(GLAD_OBJ)
	@$(CC) $(FLG) $(GLFW_STUFF) $(LIBFT_PATH)$(LIBFT_NAME) $(LIBMATHFT_PATH)$(LIBMATHFT_NAME) $(OBJ) $(GLAD_OBJ) -o $(NAME)
	@$(ECHO) $(NAME) compilation done

clean:
	@/bin/rm -rf $(OBJ_PATH)
	@make -C $(LIBFT_PATH) clean
	@make -C $(LIBMATHFT_PATH) clean
	@$(ECHO) $(NAME) clean done

fclean: clean
	@/bin/rm -f $(NAME)
	@make -C $(LIBFT_PATH) fclean
	@make -C $(LIBMATHFT_PATH) fclean
	@$(ECHO) $(NAME) fclean done

re: fclean all

PHONY : re all clean fclean
