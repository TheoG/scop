/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 11:16:50 by tgros             #+#    #+#             */
/*   Updated: 2018/04/23 12:37:51 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCOP_SCOP_H
# define SCOP_SCOP_H

# include "libft.h"
# include "libmathft.h"
# include "bmp_infos.h"
# include "glad/glad.h"

# include <errno.h>
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <stdbool.h>
# include <GLFW/glfw3.h>

# define WIDTH 1280
# define HEIGHT 720

typedef struct	s_face {
	int	p1;
	int	p2;
	int	p3;
}				t_face;

typedef struct	s_open_gl {
	unsigned	vertex_shader;
	unsigned	fragment_shader;
	unsigned	shader_program;

	unsigned	vao;
	unsigned	vbo;
	unsigned	ebo;

	unsigned	model_loc;
	unsigned	view_loc;
	unsigned	projection_loc;
	unsigned	shift_loc;
	unsigned	ratio_loc;
	unsigned	texture_app_loc;

	unsigned	texture_id[4];
}				t_open_gl;

typedef struct	s_scop {
	char		*obj_name;
	t_list		*vertices;
	t_list		*l_vertices;
	t_list		*uv_coord;
	t_list		*normal_coord;
	t_list		*faces;
	t_list		*uv_indices;
	t_list		*nor_indices;
	float		*triangles;
	t_vec3		shift;
	t_vec3		bck;
	t_vec3		obj_rotation;
	short		current_texture;
	short		triangle_texture_projection;

	GLFWwindow	*window;
	t_matrix4	model;
	t_matrix4	view;
	t_matrix4	projection;

	int			nb_triangles;
	double		last_time;

	float		ratio_texture_color;
	short		on_ratio_change;

	bool		is_rotating;
	float		move_speed;

	t_open_gl	open_gl;

	t_list		*f_end;
	t_list		*uv_end;
	t_list		*vt_end;
}				t_scop;

typedef struct	s_face_parser {
	t_pt4	idx;
	t_pt4	uv;
	t_pt4	nor;
}				t_face_parser;

/*
**	Parsing
*/

int				read_obj(t_scop *scop);
int				read_it_8(t_scop *scop, t_face_parser *face);
int				read_iun_9(t_scop *scop, t_face_parser *face);
int				read_iun_12(t_scop *scop, t_face_parser *face);
int				read_it_6(t_scop *scop, t_face_parser *face);
int				read_in_6(t_scop *scop, t_face_parser *face);
int				read_i_3(t_scop *scop, t_face_parser *face);
int				read_i_4(t_scop *scop, t_face_parser *face);
t_vec3			get_vertices_at(t_scop *scop, int i);
t_vec2			get_uv_at(t_list *vertices, int i);
void			scan_vertices(t_scop *scop, char *l, t_list **v, t_list **l_v);
void			scan_texture_vertices(t_scop *scop, char *line);
int				scan_face(t_scop *scop, char *line);
t_vec3			find_middle(t_scop *scop);
void			prepare_triangles(t_scop *scop);

/*
** Functions
*/

void			scop(t_scop *scop);
void			init_buffers(t_scop *scop, int mem_size);
void			init_matrices(t_scop *scop);
void			init_textures(t_scop *scop);
void			update_matrice_uniform(t_scop *scop);
void			loop(t_scop *scop);
void			free_matrices(t_scop *scop);

/*
** Shaders
*/

bool			open_shader(t_open_gl *env);
bool			link_shader(t_open_gl *env);

/*
** Textures
*/

t_rgb			*read_bmp(char *file_name, t_vec3 *dim);
unsigned		generate_texture(t_rgb *raw, t_vec3 *dim);

/*
** IO
*/

void			init_window(t_scop *scop);
void			process_input(t_scop *scop);
void			key_callback(GLFWwindow *win, int k, int sc, int act, int mod);
void			mouse_callback(GLFWwindow *win, int button, int act, int mods);

/*
** Utils
*/

void			error_exit(const char *message);
void			update_window_title(t_scop *scop);

#endif
