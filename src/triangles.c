/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   triangles.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 10:59:12 by tgros             #+#    #+#             */
/*   Updated: 2018/04/19 10:59:13 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void	prepare_x_vertices(t_scop *scop, int i, t_list *tmp)
{
	t_vec3			v3;

	v3 = get_vertices_at(scop, ((t_pt3*)tmp->content)->x);
	if (isnan(v3.x) || isnan(v3.y) || isnan(v3.z))
		error_exit("Error: Face index doesn't match any vertex.");
	scop->triangles[i] = v3.x;
	scop->triangles[i + 1] = v3.y;
	scop->triangles[i + 2] = v3.z;
	scop->triangles[i + 3] = 0.0f;
	scop->triangles[i + 4] = 0.0f;
	scop->triangles[i + 5] = (float)rand() / (float)RAND_MAX / 2.0f;
	scop->triangles[i + 6] = scop->triangles[i + 5];
	scop->triangles[i + 7] = scop->triangles[i + 5];
}

void	prepare_y_vertices(t_scop *scop, int i, t_list *tmp)
{
	t_vec3			v3;

	v3 = get_vertices_at(scop, ((t_pt3*)tmp->content)->y);
	if (isnan(v3.x) || isnan(v3.y) || isnan(v3.z))
		error_exit("Error: Face index doesn't match any vertex.");
	scop->triangles[i + 8] = v3.x;
	scop->triangles[i + 9] = v3.y;
	scop->triangles[i + 10] = v3.z;
	scop->triangles[i + 11] = 1.0f;
	scop->triangles[i + 12] = 0.0f;
	scop->triangles[i + 13] = scop->triangles[i + 5];
	scop->triangles[i + 14] = scop->triangles[i + 5];
	scop->triangles[i + 15] = scop->triangles[i + 5];
}

void	prepare_z_vertices(t_scop *scop, int i, t_list *tmp)
{
	t_vec3			v3;

	v3 = get_vertices_at(scop, ((t_pt3*)tmp->content)->z);
	if (isnan(v3.x) || isnan(v3.y) || isnan(v3.z))
		error_exit("Error: Face index doesn't match any vertex.");
	scop->triangles[i + 16] = v3.x;
	scop->triangles[i + 17] = v3.y;
	scop->triangles[i + 18] = v3.z;
	scop->triangles[i + 19] = 0.5f;
	scop->triangles[i + 20] = 1.0f;
	scop->triangles[i + 21] = scop->triangles[i + 5];
	scop->triangles[i + 22] = scop->triangles[i + 5];
	scop->triangles[i + 23] = scop->triangles[i + 5];
}

void	prepare_triangles(t_scop *scop)
{
	t_list			*tmp;
	unsigned		i;

	tmp = scop->faces;
	i = 0;
	while (tmp)
	{
		prepare_x_vertices(scop, i, tmp);
		prepare_y_vertices(scop, i, tmp);
		prepare_z_vertices(scop, i, tmp);
		tmp = tmp->next;
		i += 24;
	}
}
