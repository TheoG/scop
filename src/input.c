/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 10:42:14 by tgros             #+#    #+#             */
/*   Updated: 2018/04/18 11:57:40 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"
#include "../inc/scop.h"

void	translate(t_scop *scop)
{
	if (glfwGetKey(scop->window, GLFW_KEY_KP_8) == GLFW_PRESS)
		scop->view = m4_trans(scop->view, new_v3(0.f, 0.f,
								scop->move_speed / 100.0));
	else if (glfwGetKey(scop->window, GLFW_KEY_KP_2) == GLFW_PRESS)
		scop->view = m4_trans(scop->view, new_v3(0.f, 0.f,
								-scop->move_speed / 100.0));
	if (glfwGetKey(scop->window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		scop->view = m4_trans(scop->view, new_v3(-scop->move_speed / 100.0,
																	0.f, 0.f));
	else if (glfwGetKey(scop->window, GLFW_KEY_LEFT) == GLFW_PRESS)
		scop->view = m4_trans(scop->view, new_v3(scop->move_speed / 100.0,
																	0.f, 0.f));
	if (glfwGetKey(scop->window, GLFW_KEY_UP) == GLFW_PRESS)
		scop->view = m4_trans(scop->view, new_v3(0.f,
											-scop->move_speed / 100.0, 0.f));
	else if (glfwGetKey(scop->window, GLFW_KEY_DOWN) == GLFW_PRESS)
		scop->view = m4_trans(scop->view, new_v3(0.f,
												scop->move_speed / 100.0, 0.f));
}

void	process_input(t_scop *scop)
{
	translate(scop);
	if (glfwGetKey(scop->window, GLFW_KEY_W) == GLFW_PRESS)
		scop->obj_rotation.x += 1;
	if (glfwGetKey(scop->window, GLFW_KEY_S) == GLFW_PRESS)
		scop->obj_rotation.x -= 1;
	if (glfwGetKey(scop->window, GLFW_KEY_D) == GLFW_PRESS)
		scop->obj_rotation.y -= 1;
	if (glfwGetKey(scop->window, GLFW_KEY_A) == GLFW_PRESS)
		scop->obj_rotation.y += 1;
	if (glfwGetKey(scop->window, GLFW_KEY_Q) == GLFW_PRESS)
		scop->obj_rotation.z -= 1;
	if (glfwGetKey(scop->window, GLFW_KEY_E) == GLFW_PRESS)
		scop->obj_rotation.z += 1;
	scop->obj_rotation.y += scop->is_rotating;
	rotate_matrix4_v3(scop->model, scop->obj_rotation);
	if (glfwGetKey(scop->window, GLFW_KEY_SPACE) == GLFW_PRESS)
		init_matrices(scop);
	if (scop->on_ratio_change)
	{
		if (scop->ratio_texture_color <= 0.00f ||
			scop->ratio_texture_color >= 1.00f)
			scop->on_ratio_change = false;
		else
			scop->ratio_texture_color += scop->on_ratio_change / 100.0f;
	}
}

void	key_callback_2(t_scop *scop, int key, int action)
{
	if (glfwGetKey(scop->window, GLFW_KEY_R) == GLFW_PRESS)
		scop->is_rotating = !scop->is_rotating;
	if ((key == GLFW_KEY_D || key == GLFW_KEY_A) && action == GLFW_PRESS)
		scop->is_rotating = false;
	if (key == GLFW_KEY_PERIOD && action == GLFW_RELEASE)
		scop->current_texture = (scop->current_texture + 1) % 4;
	if (key == GLFW_KEY_COMMA && action == GLFW_RELEASE)
		scop->current_texture = scop->current_texture == 0 ? 3 :
								(scop->current_texture - 1) % 4;
	if (key == GLFW_KEY_P && action == GLFW_RELEASE)
		scop->triangle_texture_projection =
				(scop->triangle_texture_projection + 1) % 4;
}

void	key_callback(GLFWwindow *win, int key, int scan, int action, int mods)
{
	GLint	polygon_mode;
	t_scop	*scop;

	scop = glfwGetWindowUserPointer(win);
	(void)scan;
	(void)mods;
	if (key == GLFW_KEY_L && action == GLFW_RELEASE)
	{
		glGetIntegerv(GL_POLYGON_MODE, &polygon_mode);
		if (polygon_mode == GL_FILL)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else if (polygon_mode == GL_LINE)
			glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else if (glfwGetKey(scop->window, GLFW_KEY_T) == GLFW_PRESS &&
										!scop->on_ratio_change)
	{
		scop->on_ratio_change = (scop->ratio_texture_color <= 0.0f ? 1 : -1);
		scop->ratio_texture_color += scop->on_ratio_change / 100.0f;
	}
	key_callback_2(scop, key, action);
}

void	mouse_callback(GLFWwindow *win, int button, int action, int mods)
{
	t_scop	*scop;

	(void)mods;
	scop = glfwGetWindowUserPointer(win);
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS
			&& scop->bck.x <= 1.0f)
	{
		scop->bck.x += 0.1;
		scop->bck.y += 0.1;
		scop->bck.z += 0.1;
	}
	else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS
			&& scop->bck.x >= 0.0f)
	{
		scop->bck.x -= 0.1;
		scop->bck.y -= 0.1;
		scop->bck.z -= 0.1;
	}
}
