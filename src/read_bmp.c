/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_bmp.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 12:08:56 by tgros             #+#    #+#             */
/*   Updated: 2018/04/18 12:11:45 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"
#include <fcntl.h>

int			check_file(char *file_name, t_bmp_header *header)
{
	int	fd;
	int ret;

	if ((fd = open(file_name, O_RDONLY)) == -1)
		return (-1);
	if ((ret = read(fd, &header->signature, sizeof(WORD))) != sizeof(WORD) ||
		(ret = read(fd, &header->file_size, sizeof(DWORD)) != sizeof(DWORD)) ||
		(ret = read(fd, &header->reserv_1, sizeof(WORD)) != sizeof(WORD)) ||
		(ret = read(fd, &header->reserv_2, sizeof(WORD)) != sizeof(WORD)) ||
		(ret = read(fd, &header->offset, sizeof(DWORD)) != sizeof(DWORD)) ||
		(ret = read(fd, &header->chunk, sizeof(DWORD)) != sizeof(DWORD)) ||
		(ret = read(fd, &header->width, sizeof(DWORD)) != sizeof(DWORD)) ||
		(ret = read(fd, &header->height, sizeof(DWORD)) != sizeof(DWORD)))
	{
		errno = EIO;
		return (-1);
	}
	if (header->signature != 0x4D42 ||
		header->file_size > 10485760)
	{
		errno = EILSEQ;
		return (-1);
	}
	return (fd);
}

int			check_file2(char *file, t_bmp_header *header, int fd, t_vec3 *dim)
{
	char	ignore[256];
	int		ret;

	if ((int)header->height < 0)
		header->height = -(int)header->height;
	close(fd);
	if ((fd = open(file, O_RDONLY)) == -1)
		return (-1);
	if (dim)
	{
		dim->x = header->width;
		dim->y = header->height;
	}
	if ((ret = read(fd, &ignore, header->offset)) != (int)header->offset)
		return (-1);
	return (0);
}

int			read_bitmap_color(t_bmp_header *header, t_rgb *texture_h, int fd)
{
	t_pt2	i;
	t_rgb	*line;

	i.x = -1;
	line = (t_rgb *)malloc(sizeof(t_rgb) * header->width);
	if (!line)
		error_exit("Malloc error.");
	while (++i.x < (int)header->height)
	{
		read(fd, line, sizeof(t_rgb) * header->width);
		i.y = -1;
		while (++i.y < (int)header->width)
		{
			texture_h[i.x * header->width + i.y].r = line[i.y].b;
			texture_h[i.x * header->width + i.y].g = line[i.y].g;
			texture_h[i.x * header->width + i.y].b = line[i.y].r;
		}
	}
	free(line);
	return (0);
}

int			alloc_memory(t_rgb **h_tex, t_bmp_header *head)
{
	if (!(*h_tex = (t_rgb *)malloc(head->width * head->height * sizeof(t_rgb))))
		return (-1);
	return (1);
}

t_rgb		*read_bmp(char *file_name, t_vec3 *dim)
{
	int				fd;
	t_rgb			*texture_h;
	t_bmp_header	header;

	texture_h = NULL;
	if ((fd = check_file(file_name, &header)) == -1)
		return (NULL);
	if (check_file2(file_name, &header, fd, dim) == -1)
		return (NULL);
	if (alloc_memory(&texture_h, &header) == -1)
		error_exit("Malloc error");
	read_bitmap_color(&header, texture_h, fd);
	close(fd);
	return (texture_h);
}
