/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture_vertices.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 16:53:13 by tgros             #+#    #+#             */
/*   Updated: 2018/04/19 13:55:54 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void		scan_texture_vertices(t_scop *scop, char *line)
{
	t_vec2	v2;

	if (sscanf(line + 2, "%f %f", &v2.x, &v2.y) != 2)
		error_exit("Error while reading the file (UV coordinates error)");
	if (!scop->uv_coord)
	{
		scop->uv_coord = ft_lstnew(&v2, sizeof(t_vec2));
		scop->vt_end = scop->uv_coord;
	}
	else
		scop->vt_end = ft_lstadd_end_fast(scop->vt_end,
								ft_lstnew(&v2, sizeof(t_vec2)));
}
