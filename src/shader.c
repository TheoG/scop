/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 11:21:33 by tgros             #+#    #+#             */
/*   Updated: 2018/04/18 13:48:18 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

char	*read_shader_file(char *file_name)
{
	FILE	*f;
	long	fsize;
	char	*string;

	f = fopen(file_name, "rb");
	if (!f)
		return (NULL);
	fseek(f, 0, SEEK_END);
	fsize = ftell(f);
	fseek(f, 0, SEEK_SET);
	if (fsize > 1000000)
		error_exit("File too big!");
	string = malloc(fsize + 1);
	if (!string)
		error_exit("Malloc error (shader)");
	fread(string, fsize, 1, f);
	fclose(f);
	string[fsize] = 0;
	return (string);
}

void	read_shader(t_open_gl *env, char *name, bool is_vert)
{
	const char	*line;
	int			success;
	char		info_log[512];
	unsigned	*shader;

	if (!(line = read_shader_file(name)))
		error_exit(is_vert ? "Can't read vertex shader file" :
							"Can't read fragment shader file");
	shader = (is_vert) ? &env->vertex_shader : &env->fragment_shader;
	*shader = (is_vert) ? glCreateShader(GL_VERTEX_SHADER) :
						glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(*shader, 1, &line, NULL);
	glCompileShader(*shader);
	glGetShaderiv(*shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(*shader, 512, NULL, info_log);
		printf("ERROR::SHADER::COMPILATION_FAILED: %s\n", info_log);
		exit(1);
	}
	free((char *)line);
}

bool	open_shader(t_open_gl *env)
{
	read_shader(env, "src/shaders/base.vert", true);
	read_shader(env, "src/shaders/base.frag", false);
	return (true);
}

bool	link_shader(t_open_gl *env)
{
	int		success;
	char	info_log[512];

	env->shader_program = glCreateProgram();
	glAttachShader(env->shader_program, env->vertex_shader);
	glAttachShader(env->shader_program, env->fragment_shader);
	glLinkProgram(env->shader_program);
	glGetProgramiv(env->shader_program, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(env->shader_program, 512, NULL, info_log);
		printf("ERROR::SHADER::LINKING_FAILED: %s\n", info_log);
	}
	glUseProgram(env->shader_program);
	glDeleteShader(env->vertex_shader);
	glDeleteShader(env->fragment_shader);
	return (true);
}
