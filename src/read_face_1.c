/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_face_1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 12:57:12 by tgros             #+#    #+#             */
/*   Updated: 2018/04/19 13:55:20 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"
#include "../inc/scop.h"

int		read_iun_12(t_scop *scop, t_face_parser *face)
{
	if (!scop->faces)
	{
		scop->faces = ft_lstnew(&face->idx, sizeof(t_pt3));
		scop->f_end = scop->faces;
	}
	else
		scop->f_end = ft_lstadd_end_fast(scop->f_end,
				ft_lstnew(&face->idx, sizeof(t_pt3)));
	if (!scop->uv_indices)
	{
		scop->uv_indices = ft_lstnew(&face->uv, sizeof(t_pt3));
		scop->uv_end = scop->uv_indices;
	}
	else
		scop->uv_end = ft_lstadd_end_fast(scop->f_end, ft_lstnew(&face->uv,
															sizeof(t_pt3)));
	face->idx.y = face->idx.x;
	scop->f_end = ft_lstadd_end_fast(scop->f_end,
						ft_lstnew(((int *)(&face->idx)) + 1, sizeof(t_pt3)));
	scop->uv_end = ft_lstadd_end_fast(scop->uv_end,
						ft_lstnew(((int *)(&face->uv)) + 1, sizeof(t_pt3)));
	return (2);
}

int		read_it_8(t_scop *scop, t_face_parser *face)
{
	if (!scop->faces)
	{
		scop->faces = ft_lstnew(&face->idx, sizeof(t_pt3));
		scop->f_end = scop->faces;
	}
	else
		scop->f_end = ft_lstadd_end_fast(scop->f_end,
						ft_lstnew(&face->idx, sizeof(t_pt3)));
	if (!scop->uv_indices)
	{
		scop->uv_indices = ft_lstnew(&face->uv, sizeof(t_pt3));
		scop->uv_end = scop->uv_indices;
	}
	else
		scop->uv_end = ft_lstadd_end_fast(scop->uv_end, ft_lstnew(&face->uv,
															sizeof(t_pt3)));
	face->idx.y = face->idx.x;
	scop->f_end = ft_lstadd_end_fast(scop->f_end,
					ft_lstnew(((int *)(&face->idx)) + 1, sizeof(t_pt3)));
	scop->uv_end = ft_lstadd_end_fast(scop->uv_indices,
					ft_lstnew(((int *)(&face->uv)) + 1, sizeof(t_pt3)));
	return (2);
}

int		read_iun_9(t_scop *scop, t_face_parser *face)
{
	if (!scop->faces)
	{
		scop->faces = ft_lstnew(&face->idx, sizeof(t_pt3));
		scop->f_end = scop->faces;
	}
	else
		scop->f_end = ft_lstadd_end_fast(scop->f_end,
						ft_lstnew(&face->idx, sizeof(t_pt3)));
	if (!scop->uv_indices)
	{
		scop->uv_indices = ft_lstnew(&face->uv, sizeof(t_pt3));
		scop->uv_end = scop->uv_indices;
	}
	else
		scop->uv_end = ft_lstadd_end_fast(scop->uv_end, ft_lstnew(&face->uv,
															sizeof(t_pt3)));
	return (1);
}

int		read_it_6(t_scop *scop, t_face_parser *face)
{
	if (!scop->faces)
	{
		scop->faces = ft_lstnew(&face->idx, sizeof(t_pt3));
		scop->f_end = scop->faces;
	}
	else
		scop->f_end = ft_lstadd_end_fast(scop->f_end,
						ft_lstnew(&face->idx, sizeof(t_pt3)));
	if (!scop->uv_indices)
	{
		scop->uv_indices = ft_lstnew(&face->uv, sizeof(t_pt3));
		scop->uv_end = scop->uv_indices;
	}
	else
		scop->uv_end = ft_lstadd_end_fast(scop->uv_end, ft_lstnew(&face->uv,
															sizeof(t_pt3)));
	return (1);
}

int		read_in_6(t_scop *scop, t_face_parser *face)
{
	if (!scop->faces)
	{
		scop->faces = ft_lstnew(&face->idx, sizeof(t_pt3));
		scop->f_end = scop->faces;
	}
	else
		scop->f_end = ft_lstadd_end_fast(scop->f_end,
				ft_lstnew(&face->idx, sizeof(t_pt3)));
	return (1);
}
