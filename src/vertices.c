/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vertices.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 15:00:13 by tgros             #+#    #+#             */
/*   Updated: 2018/04/19 11:12:28 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

t_vec3		find_in_sublist(int i, t_list *l_tmp)
{
	int		j;
	t_list	*vl_tmp;
	t_vec3	*v_tmp;

	j = 0;
	v_tmp = NULL;
	if (!l_tmp)
		return (new_v3(NAN, NAN, NAN));
	vl_tmp = l_tmp->content;
	while (vl_tmp)
	{
		if ((i % 1000) == j)
			break ;
		j++;
		vl_tmp = vl_tmp->next;
	}
	if (i % 1000 == j && vl_tmp)
	{
		v_tmp = (t_vec3 *)vl_tmp->content;
		return (*v_tmp);
	}
	return (new_v3(NAN, NAN, NAN));
}

t_vec3		get_vertices_at(t_scop *scop, int i)
{
	int		l_index;
	t_list	*l_tmp;
	int		j;

	i--;
	l_index = i / 1000;
	l_tmp = scop->l_vertices;
	j = 0;
	while (l_tmp)
	{
		if (l_index == j)
			break ;
		j++;
		l_tmp = l_tmp->next;
	}
	if (l_index == j)
		return (find_in_sublist(i, l_tmp));
	return (new_v3(NAN, NAN, NAN));
}

void		init_list(t_scop *scop, t_list **v_end, t_list **l_v_end, t_vec3 v1)
{
	scop->vertices = ft_lstnew(&v1, sizeof(t_vec3));
	*v_end = scop->vertices;
	if (!(scop->l_vertices = (t_list *)malloc(sizeof(t_list))))
		error_exit("Malloc error");
	scop->l_vertices->content = scop->vertices;
	scop->l_vertices->next = NULL;
	*l_v_end = scop->l_vertices;
}

void		scan_vertices(t_scop *scop, char *line, t_list **v_end,
		t_list **l_v_end)
{
	int			ret;
	t_vec3		v1;
	static int	nb_vertices = 0;

	ret = sscanf(line + 1, "%f %f %f", &v1.x, &v1.y, &v1.z);
	if (ret != 3)
		error_exit("Error while reading the file.");
	if (!scop->vertices)
		init_list(scop, v_end, l_v_end, v1);
	else
	{
		if (nb_vertices % 1000 == 0)
		{
			(*l_v_end)->next = (t_list *)malloc(sizeof(t_list));
			(*l_v_end)->next->content = ft_lstnew(&v1, sizeof(t_vec3));
			(*l_v_end)->next->next = NULL;
			*v_end = (*l_v_end)->next->content;
			*l_v_end = (*l_v_end)->next;
		}
		else
			*v_end = ft_lstadd_end_fast(*v_end, ft_lstnew(&v1, sizeof(t_vec3)));
	}
	nb_vertices++;
}

t_vec2		get_uv_at(t_list *vertices, int i)
{
	t_list	*tmp;
	t_vec2	*vec_tmp;
	int		j;

	tmp = vertices;
	vec_tmp = NULL;
	j = 0;
	while (tmp && j != i)
	{
		vec_tmp = (t_vec2*)tmp->content;
		tmp = tmp->next;
		j++;
	}
	if (j == i)
		return (*vec_tmp);
	return (new_v2(NAN, NAN));
}
