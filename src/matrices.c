/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrices.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 11:18:16 by tgros             #+#    #+#             */
/*   Updated: 2018/04/18 13:36:24 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"
#include "../inc/scop.h"

void	free_matrices(t_scop *scop)
{
	if (scop->model)
		free(scop->model);
	if (scop->view)
		free(scop->view);
	if (scop->projection)
		free(scop->projection);
}

void	init_matrices(t_scop *scop)
{
	free_matrices(scop);
	scop->model = new_identity_matrix4();
	scop->view = new_identity_matrix4();
	scop->view = m4_trans(scop->view, new_v3(0.f, 0.f, -5.f));
	if (scop->projection)
		scop->view[14] = scop->move_speed;
	scop->projection = new_perspective_matrix4(45.0f, (float)WIDTH /
												(float)HEIGHT, 0.1f, 100.f);
	bzero(&scop->obj_rotation, sizeof(t_vec3));
}

void	update_matrice_uniform(t_scop *scop)
{
	scop->open_gl.model_loc = glGetUniformLocation(
				scop->open_gl.shader_program, "model");
	glUniformMatrix4fv(scop->open_gl.model_loc, 1, GL_FALSE, scop->model);
	scop->open_gl.view_loc = glGetUniformLocation(
				scop->open_gl.shader_program, "view");
	glUniformMatrix4fv(scop->open_gl.view_loc, 1, GL_FALSE, scop->view);
	scop->open_gl.projection_loc = glGetUniformLocation(
				scop->open_gl.shader_program, "projection");
	glUniformMatrix4fv(scop->open_gl.projection_loc, 1, GL_FALSE,
											scop->projection);
}
