/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 11:19:45 by tgros             #+#    #+#             */
/*   Updated: 2018/04/18 13:38:04 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void	init_buffers(t_scop *scop, int memory_size)
{
	glGenBuffers(1, &scop->open_gl.vbo);
	glGenVertexArrays(1, &scop->open_gl.vao);
	glBindVertexArray(scop->open_gl.vao);
	glBindBuffer(GL_ARRAY_BUFFER, scop->open_gl.vbo);
	glBufferData(GL_ARRAY_BUFFER, memory_size, scop->triangles, GL_STATIC_DRAW);
	glEnable(GL_DEPTH_TEST);
	free(scop->triangles);
}

void	scop(t_scop *scop)
{
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), 0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
											(void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
											(void*)(5 * sizeof(float)));
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	init_textures(scop);
	glUniform1i(glGetUniformLocation(scop->open_gl.shader_program, "tex"), 0);
	glUseProgram(scop->open_gl.shader_program);
	scop->last_time = glfwGetTime();
	loop(scop);
	glfwTerminate();
	free_matrices(scop);
}
