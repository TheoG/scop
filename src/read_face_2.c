/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_face_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 13:00:00 by tgros             #+#    #+#             */
/*   Updated: 2018/04/19 11:33:19 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

int		read_i_3(t_scop *scop, t_face_parser *face)
{
	if (!scop->faces)
	{
		scop->faces = ft_lstnew(&face->idx, sizeof(t_pt3));
		scop->f_end = scop->faces;
	}
	else
		scop->f_end = ft_lstadd_end_fast(scop->f_end,
				ft_lstnew(&face->idx, sizeof(t_pt3)));
	return (1);
}

int		read_i_4(t_scop *scop, t_face_parser *face)
{
	if (!scop->faces)
	{
		scop->faces = ft_lstnew(&face->idx, sizeof(t_pt3));
		scop->f_end = scop->faces;
	}
	else
		scop->f_end = ft_lstadd_end_fast(scop->f_end,
				ft_lstnew(&face->idx, sizeof(t_pt3)));
	face->idx.y = face->idx.x;
	scop->f_end = ft_lstadd_end_fast(scop->f_end,
			ft_lstnew(((int *)(&face->idx)) + 1, sizeof(t_pt3)));
	return (2);
}

int		scan_face_2(t_scop *scop, char *line)
{
	t_face_parser	face;
	int				nb_faces;

	nb_faces = 0;
	if (sscanf(line + 1, "%d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d",
				&face.idx.x, &face.uv.x, &face.nor.x, &face.idx.y, &face.uv.y,
				&face.nor.y, &face.idx.z, &face.uv.z, &face.nor.z,
				&face.idx.w, &face.uv.w, &face.nor.w) == 12)
		return (read_iun_12(scop, &face));
	else if (sscanf(line + 1, "%d/%d/%d %d/%d/%d %d/%d/%d",
				&face.idx.x, &face.uv.x, &face.nor.x, &face.idx.y, &face.uv.y,
				&face.nor.y, &face.idx.z, &face.uv.z, &face.nor.z) == 9)
		return (read_iun_9(scop, &face));
	else if (sscanf(line + 1, "%d/%d %d/%d %d/%d %d/%d",
				&face.idx.x, &face.uv.x, &face.idx.y, &face.uv.y, &face.idx.z,
				&face.uv.z, &face.idx.w, &face.uv.w) == 8)
		return (read_it_8(scop, &face));
	return (0);
}

int		scan_face(t_scop *scop, char *line)
{
	t_face_parser	face;
	int				nb_faces;

	nb_faces = scan_face_2(scop, line);
	if (nb_faces)
		return (nb_faces);
	else if (sscanf(line + 1, "%d//%d %d//%d %d//%d",
				&face.idx.x, &face.nor.x, &face.idx.y, &face.nor.y,
									&face.idx.z, &face.nor.z) == 6)
		nb_faces = read_in_6(scop, &face);
	else if (sscanf(line + 1, "%d/%d %d/%d %d/%d",
				&face.idx.x, &face.uv.x, &face.idx.y, &face.uv.y, &face.idx.z,
															&face.uv.z) == 6)
		nb_faces = read_it_6(scop, &face);
	else if (sscanf(line + 1, "%d %d %d %d", &face.idx.x, &face.idx.y,
									&face.idx.z, &face.idx.w) == 4)
		nb_faces = read_i_4(scop, &face);
	else if (sscanf(line + 1, "%d %d %d", &face.idx.x, &face.idx.y,
													&face.idx.z) == 3)
		nb_faces = read_i_3(scop, &face);
	return (nb_faces);
}
