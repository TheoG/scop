/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 11:32:59 by tgros             #+#    #+#             */
/*   Updated: 2018/04/17 11:33:18 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"
#include "../inc/scop.h"

void	update_window_title(t_scop *scop)
{
	char		title[128];
	double		current_time;
	static int	nb_frames = 0;

	nb_frames++;
	current_time = glfwGetTime();
	if (current_time - scop->last_time >= 1.0)
	{
		sprintf(title, "Scop - %.2f ms (%.2f FPS)",
			1000.0 / (float)(nb_frames), (float)(nb_frames));
		glfwSetWindowTitle(scop->window, title);
		nb_frames = 0;
		scop->last_time += 1.0;
	}
}

void	window_size_callback(GLFWwindow *window, int width, int height)
{
	t_scop	*scop;
	float	fov;

	scop = glfwGetWindowUserPointer(window);
	fov = (45.0f / 180.0f) * M_PI;
	if (scop->projection)
		scop->projection[0] = 1.0 / (width / (float)height * tan(fov / 2));
}

void	init_window(t_scop *scop)
{
	if (!glfwInit())
		error_exit("Can't load glfw");
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	scop->window = glfwCreateWindow(WIDTH, HEIGHT, "Scop", NULL, NULL);
	if (!scop->window)
	{
		glfwTerminate();
		error_exit("Unable to load a window");
	}
	glfwMakeContextCurrent(scop->window);
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		error_exit("Glad error while loading");
	glfwSetWindowUserPointer(scop->window, scop);
	glfwSetKeyCallback(scop->window, key_callback);
	glfwSetMouseButtonCallback(scop->window, mouse_callback);
	glfwSetWindowSizeCallback(scop->window, window_size_callback);
	scop->bck.x = 0.3f;
	scop->bck.y = 0.3f;
	scop->bck.z = 0.3f;
	scop->ratio_texture_color = 1.0f;
	scop->is_rotating = true;
}
