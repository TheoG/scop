/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   center_obj.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 15:34:58 by tgros             #+#    #+#             */
/*   Updated: 2018/04/22 16:37:26 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

t_vec3		calculate_result(t_scop *scop, t_vec3 min, t_vec3 max)
{
	float	temp;
	t_vec3	min_dist;
	t_vec3	res;

	res = new_v3((max.x + min.x) / 2.0, (max.y + min.y) / 2.0,
										(max.z + min.z) / 2.0);
	max.x = fabs(max.x) - res.x;
	max.y = fabs(max.y) - res.y;
	max.z = fabs(max.z) - res.z;
	min.x = fabs(min.x) - res.x;
	min.y = fabs(min.y) - res.y;
	min.z = fabs(min.z) - res.z;
	min_dist = new_v3(max.x + min.x, max.y + min.y, max.z + min.z);
	temp = (min_dist.x < min_dist.y) ? min_dist.y : min_dist.x;
	scop->view[14] -= (min_dist.z < temp) ? temp : min_dist.z;
	scop->move_speed = scop->view[14];
	return (res);
}

void		min_max_in_sublist(t_vec3 *min, t_vec3 *max, t_list *vertices)
{
	while (vertices)
	{
		if (((t_vec3*)vertices->content)->x < min->x)
			min->x = ((t_vec3*)vertices->content)->x;
		else if (((t_vec3*)vertices->content)->x > max->x)
			max->x = ((t_vec3*)vertices->content)->x;
		if (((t_vec3*)vertices->content)->y < min->y)
			min->y = ((t_vec3*)vertices->content)->y;
		else if (((t_vec3*)vertices->content)->y > max->y)
			max->y = ((t_vec3*)vertices->content)->y;
		if (((t_vec3*)vertices->content)->z < min->z)
			min->z = ((t_vec3*)vertices->content)->z;
		else if (((t_vec3*)vertices->content)->z > max->z)
			max->z = ((t_vec3*)vertices->content)->z;
		vertices = vertices->next;
	}
}

t_vec3		find_middle(t_scop *scop)
{
	t_list	*l_v;
	t_list	*tmp;
	t_vec3	min;
	t_vec3	max;

	l_v = scop->l_vertices;
	tmp = scop->vertices;
	min.x = INFINITY;
	min.y = INFINITY;
	min.z = INFINITY;
	max.x = -INFINITY;
	max.y = -INFINITY;
	max.z = -INFINITY;
	while (l_v)
	{
		min_max_in_sublist(&min, &max, l_v->content);
		l_v = l_v->next;
	}
	return (calculate_result(scop, min, max));
}
