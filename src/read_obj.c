/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_obj.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 12:16:58 by tgros             #+#    #+#             */
/*   Updated: 2018/04/19 13:55:35 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"
#include "../inc/scop.h"

void	clean_parsing(t_scop *scop)
{
	t_list	*list_vertices;
	t_list	*vertices;
	t_list	*to_remove;

	list_vertices = scop->l_vertices;
	while (list_vertices)
	{
		vertices = list_vertices->content;
		while (vertices)
		{
			to_remove = vertices;
			free(to_remove->content);
			vertices = vertices->next;
			free(to_remove);
		}
		to_remove = list_vertices;
		list_vertices = list_vertices->next;
		free(to_remove);
	}
}

void	read_file(t_scop *scop, FILE *fp, unsigned *nb_faces)
{
	char	*line;
	size_t	len;
	char	key[1024];
	t_list	*v_end;
	t_list	*l_v_end;

	line = NULL;
	len = 0;
	while (getline(&line, &len, fp) != -1)
	{
		bzero(key, 1024);
		if (len >= 1024)
			error_exit("I think this line is too long for an obj file.");
		sscanf(line, "%s", key);
		if (strcmp(key, "v") == 0)
			scan_vertices(scop, line, &v_end, &l_v_end);
		else if (strcmp(key, "vt") == 0)
			scan_texture_vertices(scop, line);
		else if (strcmp(key, "f") == 0)
			*nb_faces += scan_face(scop, line);
	}
	if (line)
		free(line);
}

void	set_uv_coordinates(t_scop *scop)
{
	t_list		*tmp;
	unsigned	i;
	t_vec2		v2;

	tmp = scop->uv_indices;
	i = 0;
	while (tmp)
	{
		v2 = get_uv_at(scop->uv_coord, ((t_pt3*)tmp->content)->x);
		scop->triangles[i + 3] = v2.x;
		scop->triangles[i + 4] = v2.y;
		v2 = get_uv_at(scop->uv_coord, ((t_pt3*)tmp->content)->y);
		scop->triangles[i + 11] = v2.x;
		scop->triangles[i + 12] = v2.y;
		v2 = get_uv_at(scop->uv_coord, ((t_pt3*)tmp->content)->z);
		scop->triangles[i + 19] = v2.x;
		scop->triangles[i + 20] = v2.y;
		tmp = tmp->next;
		i += 24;
	}
}

int		read_obj(t_scop *scop)
{
	FILE		*fp;
	unsigned	nb_faces;

	fp = fopen(scop->obj_name, "r");
	if (fp == NULL)
	{
		perror(scop->obj_name);
		exit(EXIT_FAILURE);
	}
	nb_faces = 0;
	read_file(scop, fp, &nb_faces);
	if (!nb_faces)
		error_exit("Parsing error.");
	printf("Rendering %d triangles...\n", nb_faces);
	scop->triangles = (float*)malloc(sizeof(float) * nb_faces * 3 * 8);
	if (!scop->triangles)
		error_exit("Malloc error");
	scop->shift = find_middle(scop);
	prepare_triangles(scop);
	set_uv_coordinates(scop);
	clean_parsing(scop);
	return (sizeof(float) * nb_faces);
}
