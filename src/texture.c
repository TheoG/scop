/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 11:30:12 by tgros             #+#    #+#             */
/*   Updated: 2018/04/17 11:30:42 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void		set_texture_settings(void)
{
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

unsigned	generate_texture(t_rgb *raw_texture, t_vec3 *dim)
{
	unsigned int texture;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, dim->x, dim->y, 0, GL_RGB,
										GL_UNSIGNED_BYTE, raw_texture);
	glGenerateMipmap(GL_TEXTURE_2D);
	free(raw_texture);
	set_texture_settings();
	return (texture);
}

void		init_textures(t_scop *scop)
{
	t_rgb		*texture;
	t_vec3		dim;

	texture = read_bmp("res/textures/norminet.bmp", &dim);
	scop->open_gl.texture_id[0] = generate_texture(texture, &dim);
	texture = read_bmp("res/textures/chatons.bmp", &dim);
	scop->open_gl.texture_id[1] = generate_texture(texture, &dim);
	texture = read_bmp("res/textures/container.bmp", &dim);
	scop->open_gl.texture_id[2] = generate_texture(texture, &dim);
	texture = read_bmp("res/textures/wall.bmp", &dim);
	scop->open_gl.texture_id[3] = generate_texture(texture, &dim);
}
