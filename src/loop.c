/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 10:52:12 by tgros             #+#    #+#             */
/*   Updated: 2018/04/19 17:05:14 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void	set_uniform(t_scop *scop)
{
	scop->open_gl.shift_loc = glGetUniformLocation(
			scop->open_gl.shader_program, "shift");
	glUniform3f(scop->open_gl.shift_loc, scop->shift.x, scop->shift.y,
			scop->shift.z);
	scop->open_gl.ratio_loc = glGetUniformLocation(
			scop->open_gl.shader_program, "ratio");
	glUniform1f(scop->open_gl.ratio_loc, scop->ratio_texture_color);
	scop->open_gl.texture_app_loc = glGetUniformLocation(
			scop->open_gl.shader_program, "texture_application");
	glUniform1i(scop->open_gl.texture_app_loc,
			scop->triangle_texture_projection);
}

void	loop(t_scop *scop)
{
	while (!glfwWindowShouldClose(scop->window) &&
			glfwGetKey(scop->window, GLFW_KEY_ESCAPE) != GLFW_PRESS)
	{
		glClearColor(scop->bck.x, scop->bck.y, scop->bck.z, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		update_window_title(scop);
		glUseProgram(scop->open_gl.shader_program);
		update_matrice_uniform(scop);
		set_uniform(scop);
		process_input(scop);
		glActiveTexture(GL_TEXTURE + scop->current_texture);
		glBindTexture(GL_TEXTURE_2D,
				scop->open_gl.texture_id[scop->current_texture]);
		glBindVertexArray(scop->open_gl.vao);
		glDrawArrays(GL_TRIANGLES, 0, scop->nb_triangles);
		glBindVertexArray(0);
		glfwSwapBuffers(scop->window);
		glfwPollEvents();
	}
}
