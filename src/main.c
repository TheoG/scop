/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 10:59:10 by tgros             #+#    #+#             */
/*   Updated: 2018/04/20 10:23:46 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"
#include <time.h>

void	print_commands(void)
{
	printf("================= SCOP =================\n");
	printf("Rotations:\n");
	printf("\tX: W and S\n");
	printf("\tY: A and D\n");
	printf("\tZ: Q and E\n\n");
	printf("Translations:\n");
	printf("\tX: Right and Left arrows\n");
	printf("\tY: Up and Down arrows\n");
	printf("\tZ: 8 and 2 Keypad\n\n");
	printf("Turn on / off auto rotate: R\n");
	printf("Turn on / off texture: T\n");
	printf("Change texture projection mode: P\n");
	printf("Display Triangle / Edges / Vertices: L\n");
	printf("Change texture: < and >\n");
	printf("Darker / Lighter the background: Right / Left click\n");
	printf("Quit: Esc\n");
	printf("========================================\n");
}

int		main(int argc, char *argv[])
{
	t_scop		scop_data;
	int			memory_size;

	srand(time(NULL));
	memory_size = 0;
	bzero(&scop_data, sizeof(t_scop));
	if (argc > 1)
	{
		scop_data.obj_name = argv[1];
		init_matrices(&scop_data);
		scop_data.nb_triangles = read_obj(&scop_data);
		memory_size = scop_data.nb_triangles * 3 * 8;
	}
	else
		error_exit("usage: ./scop file.obj");
	init_window(&scop_data);
	open_shader(&scop_data.open_gl);
	link_shader(&scop_data.open_gl);
	init_buffers(&scop_data, memory_size);
	print_commands();
	scop(&scop_data);
	return (0);
}
