/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 17:56:25 by tgros             #+#    #+#             */
/*   Updated: 2018/04/18 13:50:57 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_end(t_list *alst, t_list *new)
{
	t_list	*head;

	head = alst;
	if (!new || !alst)
		return ;
	while (head->next)
		head = head->next;
	head->next = new;
	new->next = NULL;
}

t_list	*ft_lstadd_end_fast(t_list *end, t_list *new)
{
	end->next = new;
	new->next = NULL;
	return (new);
}
